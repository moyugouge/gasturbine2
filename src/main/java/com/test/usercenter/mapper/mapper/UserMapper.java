package com.test.usercenter.mapper.mapper;

import com.test.usercenter.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 10022
* @description 针对表【user(用户)】的数据库操作Mapper
* @createDate 2023-10-10 15:43:44
* @Entity com.test.usercenter.model.User
*/
@Mapper
public interface UserMapper extends BaseMapper<User> {

}




