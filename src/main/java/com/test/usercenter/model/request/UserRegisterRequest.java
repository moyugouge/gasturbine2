package com.test.usercenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户注册请求体
 */
@Data
//继承序列化
public class UserRegisterRequest implements Serializable {

   private static final long serialVersionUID = 3191241716373120793L;

    private String userAccount;
    private String userPassword;
    private String userCode;
    private String checkPassword;


}
