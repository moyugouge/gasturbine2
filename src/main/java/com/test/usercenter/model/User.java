package com.test.usercenter.model;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户
 * @TableName user
 * MybatisX自动生成对应的类，xml，service层
 */
@TableName(value ="user")
@Data
public class User implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 
     */
    private String username;

    /**
     *
     */
    private String userAccount;

    /**
     * 账号
     */
    private String userPassword;

    /**
     * 密码
     */
    private String phone;

    /**
     * 用户状态 0正常
     */
    private Integer userStatus;

    /**
     * 角色 0为普通用户，1为管理员
     */
    private Integer userRole;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 
     */
    private Date createTime;

    private String avatarUrl;

    private  String userCode;

    /**
     * 给mybatisplus来辨认出是用来逻辑删除的
     */
    @TableLogic
    private Integer isDelete;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}