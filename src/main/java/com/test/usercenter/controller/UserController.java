package com.test.usercenter.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.test.usercenter.commom.BaseResponse;
import com.test.usercenter.commom.ErrorCode;
import com.test.usercenter.commom.ResultUtils;
import com.test.usercenter.exception.BusinessException;
import com.test.usercenter.model.User;
import com.test.usercenter.model.request.UserLoginRequest;
import com.test.usercenter.model.request.UserRegisterRequest;
import com.test.usercenter.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.test.usercenter.constant.UserConstant.ADMIN_ROLE;
import static com.test.usercenter.constant.UserConstant.USER_LOGIN_STATE;

/**
 * 用户接口
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private UserService userService;

    @PostMapping("/register")
    public BaseResponse<Long> userRegister(@RequestBody UserRegisterRequest userRegisterRequest){
        if(userRegisterRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        String userAccount = userRegisterRequest.getUserAccount();
        String userPassword = userRegisterRequest.getUserPassword();
        String checkPassword = userRegisterRequest.getCheckPassword();
        String userCode =userRegisterRequest.getUserCode();
        //在controller里先校验一次，如果失败不进入service层（service层还会校验）
        /**
         * controller层倾向于对请求参数本身的校验，不涉及业务逻辑本身（越少越好）
         * service层是对业务逻辑的校验（有可能被controller以外的类调用）
         */
        if(StringUtils.isAnyBlank(userAccount,userPassword,checkPassword,userCode)){
            return null;
        }

        long result = userService.userRegister(userAccount,userPassword,checkPassword,userCode);
        return ResultUtils.success(result);

    }

    @PostMapping("/login")
    public BaseResponse<User> userLogin(@RequestBody UserLoginRequest userLoginRequest, HttpServletRequest request){
        if(userLoginRequest == null){
            return ResultUtils.error(ErrorCode.PARAMS_ERROR);
        }

        String userAccount = userLoginRequest.getUserAccount();
        String userPassword = userLoginRequest.getUserPassword();

        if(StringUtils.isAnyBlank(userAccount,userPassword)){
            return ResultUtils.error(ErrorCode.PARAMS_ERROR);
        }

        User user= userService.userLogin(userAccount,userPassword,request);
        return ResultUtils.success(user);
    }
    //注销
    @PostMapping("/logout")
    public BaseResponse<Integer> userLogout(HttpServletRequest request) {
        if (request == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        int result = userService.userLogout(request);
        return ResultUtils.success(result);
    }


    //获取用户当前状态信息
    @GetMapping("/current")
    public BaseResponse<User>  getCurrentUser(HttpServletRequest request){
        Object userObj = request.getSession().getAttribute(USER_LOGIN_STATE);
        User currentuser = (User)userObj;
        if(currentuser==null){
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        long userId=currentuser.getId();
        User user = userService.getById(userId);
        User safetyUser = userService.getSafetyUser(user);
        return ResultUtils.success(safetyUser);

    }
//todo 由于封装后，前端对应不上
    @GetMapping("/search")
    public BaseResponse<List<User>> searchUser(String username,HttpServletRequest request){
        //仅管理员可查询
        if (!isAdmin(request)) {
            throw new BusinessException(ErrorCode.NO_AUTH, "缺少管理员权限");
        }

        QueryWrapper<User> queryWrapper =new QueryWrapper<>();
        if (StringUtils.isNotBlank(username)){
            queryWrapper.like("username",username);
        }
        List<User> userList = userService.list(queryWrapper);
        List<User> list = userList.stream().map(user -> userService.getSafetyUser(user)).collect(Collectors.toList());
        return ResultUtils.success(list);



    }
    @PostMapping("/delete")
    public BaseResponse<Boolean> deleteUser(@RequestBody long id ,HttpServletRequest request){
        //仅管理员可删除
        if (!isAdmin(request)) {
            throw new BusinessException(ErrorCode.NO_AUTH);
        }
        if (id <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        boolean b = userService.removeById(id);
        return ResultUtils.success(b);



    }
    private boolean isAdmin(HttpServletRequest request) {
        // 仅管理员可查询
        Object userObj = request.getSession().getAttribute("userLoginState");
        User user = (User) userObj;
        return user != null && user.getUserRole() == 1;
    }


}
