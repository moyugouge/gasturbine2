package com.test.usercenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.usercenter.commom.ErrorCode;
import com.test.usercenter.exception.BusinessException;
import com.test.usercenter.service.UserService;
import com.test.usercenter.model.User;
import com.test.usercenter.mapper.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.test.usercenter.constant.UserConstant.USER_LOGIN_STATE;

/**
 * @author 10022
 * @description 针对表【user(用户)】的数据库操作Service实现
 * @createDate 2023-10-10 15:43:44
 *
 * 用户实现类
 */
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {
    @Autowired
    private UserMapper userMapper;

    //言值，用来混淆密码
    private static final String SALT = "yingyu";

    @Override
    public long userRegister(String userAccount, String userPassword, String checkPassword,String userCode) {

        //1.校验,使用commons-lang3包下的stringutils方法实现多个字符串判断是否为空（值为空，空字符串）
        if(StringUtils.isAnyBlank(userAccount,userPassword,checkPassword,userCode)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "参数为空");
        }
        if(userAccount.length()<4){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户账号过短");
        }
        if(userPassword.length()<8 ||checkPassword.length()<8){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户密码过短");
        }
        if(userCode.length()<1 ||userCode.length()>20){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "编号过长");
        }

        //账户不能包含特殊字符
        String validPattern = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Matcher matcher = Pattern.compile(validPattern).matcher(userAccount);
        if(matcher.find()){
            return -1;
        }
        //密码和校验密码相同
        if(!userPassword.equals(checkPassword)){
            return -1;
        }

        //账户不能重复
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userAccount",userAccount);
        long count = userMapper.selectCount(queryWrapper);
        if (count>0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号重复");
        }
        //编号不能重复
        QueryWrapper<User> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("code",userCode);
        long count2 = userMapper.selectCount(queryWrapper);
        if (count>0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "编号重复");
        }

        //加密
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());

        //插入数据
        User user = new User();
        user.setUserAccount(userAccount);
        user.setUserPassword(encryptPassword);
        user.setUserCode(userCode);
        boolean saveResult=this.save(user);//service自带的方法，如果用mapper中的insert返回的时int
        if(!saveResult){
            return -1;
        }

        //因为这边返回的id时long，而user类里是Long，所以当返回为空值时会报错
        return user.getId();


    }
//todo 要改回去
    @Override
    public User userLogin(String userAccount, String userPassword, HttpServletRequest request) {
        if(StringUtils.isAnyBlank(userAccount,userPassword)){
            return null;
        }
        if(userAccount.length()<4){
            return null;
        }
        if(userPassword.length()<8){
            return null;
        }

        //账户不能包含特殊字符
        String validPattern = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
        Matcher matcher = Pattern.compile(validPattern).matcher(userAccount);
        if(matcher.find()){
            return null;
        }


        //加密
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());
        //查询用户是否存在
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userAccount",userAccount);
        queryWrapper.eq("userPassword",encryptPassword);
        User user = userMapper.selectOne(queryWrapper);
        //用户不存在
        if(user == null){
            log.info("user login failed,userAccount can not match userPassword");
            return null;
        }

        //用户脱敏

        User safetyUser = getSafetyUser(user);
        //记录用户登录态
        request.getSession().setAttribute(USER_LOGIN_STATE,safetyUser);
        return safetyUser;


    }
    @Override
    public User getSafetyUser(User orginUser){
        if(orginUser==null){
            return null;
        }
        User safetyUser = new User();
        safetyUser.setId(orginUser.getId());
        safetyUser.setUsername(orginUser.getUsername());
        safetyUser.setUserAccount(orginUser.getUserAccount());
        safetyUser.setPhone(orginUser.getPhone() );
        safetyUser.setUserRole(orginUser.getUserRole());
        safetyUser.setUserStatus(0);
        safetyUser.setCreateTime(orginUser.getCreateTime());
        safetyUser.setAvatarUrl(orginUser.getAvatarUrl());
        safetyUser.setUserCode(orginUser.getUserCode());
        return safetyUser;

    }

    @Override
    public int userLogout(HttpServletRequest request) {
        // 移除登录态
        request.getSession().removeAttribute(USER_LOGIN_STATE);
        return 1;
    }

}




