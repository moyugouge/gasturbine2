package com.test.usercenter.service;

import com.test.usercenter.model.User;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
* @author 10022
* @description 针对表【user(用户)】的数据库操作Service
* @createDate 2023-10-10 15:43:44
*/

//按住alt+enter,create test,创建UserServiceTest测试类，测试mybatisX是否成功引入
public interface UserService extends IService<User> {
    /**
     *用户注释
     *
     * @param userAccount 用户账户
     * @param userPassword  用户密码
     * @param checkPassword 校验密码
     * @return  返回用户新id
     */


    //对着userRegister alt+enter 点一下implement method，自动生成对应实现类
    long userRegister(String userAccount,String userPassword,String checkPassword,String userCode);


    /**
     * 用户登录
     *
     * @param userAccount  用户账户
     * @param userPassword 用户密码
     * @param request
     * @return 脱敏后的用户信息
     */
    User userLogin(String userAccount, String userPassword, HttpServletRequest request);


    /**
     *用户脱敏
     * @param orginUser
     * @return
     */
    User getSafetyUser(User orginUser);
    /**
     * 用户注销
     *
     * @param request
     * @return
     */
    int userLogout(HttpServletRequest request);
}


