package com.test.usercenter.service;
import java.util.Date;

import com.test.usercenter.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

//用户测试
@SpringBootTest
class UserServiceTest   {

    @Resource
    private UserService userService;

    @Test
    //配置插件generateallsetter，来自动生成set方法，再创建的对象上alt+enter
    public void testAddUser(){
        User user=new User();
        user.setUsername("dogboy");
        user.setUserAccount("123");
        user.setUserPassword("xxx");
        user.setPhone("123");
        user.setUserStatus(0);
        user.setUpdateTime(new Date());
        user.setCreateTime(new Date());
        user.setIsDelete(0);
        boolean result=userService.save(user);
        System.out.println(user.getId());
        Assertions.assertTrue(result);



    }

    @Test
    void userRegister() {
        String userAccount = "1234556";
        String userPassword = "";
        String checkPassword = "12345678";
        String code ="123";
        long result = userService.userRegister(userAccount,userPassword,checkPassword,code);
        Assertions.assertEquals(-1,result);

        userAccount = "12";
        result = userService.userRegister(userAccount,userPassword,checkPassword,code);
        Assertions.assertEquals(-1,result);

        userAccount ="1234556";
        userPassword ="12345";
        result = userService.userRegister(userAccount,userPassword,checkPassword,code);
        Assertions.assertEquals(-1,result);

        userAccount ="12,34";
        userPassword = "12345678";
        result = userService.userRegister(userAccount,userPassword,checkPassword,code);
        Assertions.assertEquals(-1,result);

        checkPassword="123456789";
        result = userService.userRegister(userAccount,userPassword,checkPassword,code);
        Assertions.assertEquals(-1,result);

        userAccount="1234";
        checkPassword="12345678";
        result = userService.userRegister(userAccount,userPassword,checkPassword,code);
        Assertions.assertEquals(-1,result);

        userAccount="1234556";
        result = userService.userRegister(userAccount,userPassword,checkPassword,code);
        //Assertions.assertTrue(result>0);




    }
}